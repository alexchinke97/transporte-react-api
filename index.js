const express = require('express');
const cors = require('cors');
const empleado = require('./routes/empleado.route');


const app = express();

//middlewares
app.use(cors());
app.use(express.json());

//routes

app.use('/api/empleado', empleado);


//deploying server
app.listen(8080,()=>{
    console.log("Servidor en linea")
})