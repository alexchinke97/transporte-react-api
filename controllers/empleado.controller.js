const sequelize = require('../database/index')

const getUsuarios = async (req,res)=>{
    try{
        const empleados = await sequelize.query('select * from empleados');
        res.json({
            data: empleados[0],
            msg: "",
            code:1
        })
    }catch(e){
        res.json({
            data: [],
            msg: "",
            code:0
        })
    }
}


const updateEmpleado = async (req,res)=>{
    const {dui} = req.params;
    const {
        nombres,
        apellidos,
        telefono,
        fechanacimiento
    } = req.body;
    const sql = `
    UPDATE empleados
    SET 
        nombres = '${nombres}',
        apellidos = '${apellidos}',
        telefono = '${telefono}',
        fechanacimiento = '${fechanacimiento}'
        
    WHERE dui = '${dui}';`
    try{
        await sequelize.query(sql);
        res.json({
            code: 1,
            msg: "Se ha actualizado exitosamente",
            data: []
        })
    }catch(e){
        res.json({
            code: 0,
            msg: "Error: " +e,
            data: []
        })
    }
}



module.exports = {
    getUsuarios,
    updateEmpleado
}