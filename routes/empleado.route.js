const {Router} = require('express');
const { getUsuarios, updateEmpleado } = require('../controllers/empleado.controller');

const router = Router();

router.get('/',getUsuarios);
router.put('/:dui',updateEmpleado)

module.exports = router;